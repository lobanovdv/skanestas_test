# skanestas_test



## Back end
    used inmemory storage. 
    DB could be integrated simple
    framework fastapi used asyncio loop
    frontend integration done using WS

### to run
from backend app folder 
activate virtual env 
pip install -r requirments.txt
python3 ./main.py 


## Front end 
    Angular application
    used library for search dropdown 
    used library for chart

### run 
move to rate-viewer folder 
ng serve 


<p>
    <img src="images/example.png" width="800" />
</p>


