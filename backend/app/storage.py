from backend.app.constance import AMOUNT_OF_TICKETS
from backend.app.utils import get_formatted_number
from threading import Lock


class Storage():
    def __init__(self):
        self._data = {}
        self.data_mutex = Lock()
        for i in range(AMOUNT_OF_TICKETS):
            self._data["ticket_" + get_formatted_number(i)] = 0

    def get_data(self):
        with self.data_mutex:
            return self._data.copy()

    def set_data(self, key: str, value: int):
        with self.data_mutex:
            self._data[key] = value
