import asyncio
import threading
from random import random


def get_formatted_number(n: int, max_digits=3) -> str:
    n_str = str(n)
    if len(n_str) > max_digits:
        raise Exception()
    return "0" * (max_digits - len(n_str)) + n_str


def generate_movement(old_value: int) -> int:
    movement = -1 if random() < 0.5 else 1
    return old_value + movement
