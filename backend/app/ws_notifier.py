import json
import logging
from typing import List

from starlette.websockets import WebSocket, WebSocketState
from collections import defaultdict
from threading import Lock


class WSNotifier:
    def __init__(self):
        self.connections: List[WebSocket] = []
        self.generator = self.get_notification_generator()
        self.subscriptions = defaultdict(lambda: [])
        self.subscriptions_mutex = Lock()

    async def get_notification_generator(self):
        while True:
            message = yield
            await self._notify(message)

    async def push(self, msg: str) -> None:
        await self.generator.asend(msg)

    async def connect(self, websocket: WebSocket) -> None:
        await websocket.accept()
        self.connections.append(websocket)

    def remove(self, websocket: WebSocket) -> None:
        self.connections.remove(websocket)

    def add_subscription(self, ticket_key: str, websocket: WebSocket) -> None:
        logging.info(f"add_subscription {ticket_key}")
        with self.subscriptions_mutex:
            self.subscriptions[ticket_key].append(websocket)

    def remove_subscription(self, ticket_key, websocket: WebSocket):
        logging.info(f"remove_subscription {ticket_key}")
        with self.subscriptions_mutex:
            self.subscriptions[ticket_key].remove(websocket)

    async def notify_tickets(self, ticket_key: str, value: int) -> None:
        with self.subscriptions_mutex:
            not_closed_ws = []
            for websocket in self.subscriptions[ticket_key]:
                if websocket.client_state == WebSocketState.DISCONNECTED:
                    continue
                not_closed_ws.append(websocket)
                logging.info(f"send_text {ticket_key} {value}")
                await websocket.send_text(json.dumps({
                    "name": ticket_key,
                    "current_rate": value
                }))
            self.subscriptions[ticket_key] = not_closed_ws

    async def _notify(self, message: str) -> None:
        living_connections = []
        while len(self.connections) > 0:
            websocket = self.connections.pop()
            await websocket.send_text(message)
            living_connections.append(websocket)
        self.connections = living_connections
