import asyncio
import json

from fastapi import FastAPI
from fastapi_utils.tasks import repeat_every

import uvicorn
from fastapi.middleware.cors import CORSMiddleware

from backend.app.constance import WS_TYPE_OPEN, WS_TYPE_CLOSE
from backend.app.models import RateMessage, InputSubscriptionMessage
from backend.app.storage import Storage
from backend.app.utils import generate_movement
from backend.app.ws_notifier import WSNotifier
from starlette.websockets import WebSocket, WebSocketDisconnect
from dacite import from_dict

app = FastAPI()
storage = Storage()
notifier = WSNotifier()
loop = asyncio.get_event_loop()

origins = [
    "http://localhost:4200"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/get_tickets")
async def get_tickets():
    data = storage.get_data()
    result = []
    for k, v in data.items():
        serialized_data = RateMessage(name=k, current_rate=v)
        result.append(serialized_data)
    return {"results": result}


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await notifier.connect(websocket)
    try:
        while True:
            data = await websocket.receive_text()
            data = from_dict(InputSubscriptionMessage, json.loads(data))
            if data.type == WS_TYPE_OPEN:
                notifier.add_subscription(data.value, websocket)
            if data.type == WS_TYPE_CLOSE:
                notifier.remove_subscription(data.value, websocket)
    except WebSocketDisconnect:
        notifier.remove(websocket)


@app.on_event("startup")
@repeat_every(seconds=1)
async def randomly_update_data() -> None:
    data = storage.get_data()
    for k, v in data.items():
        new_value = generate_movement(v)
        storage.set_data(k, new_value)
        asyncio.create_task(notifier.notify_tickets(k, new_value))


if __name__ == "__main__":
    uvicorn.run(app, host="localhost", port=8000, loop="asyncio")
