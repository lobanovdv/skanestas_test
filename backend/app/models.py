from dataclasses import dataclass


@dataclass
class RateMessage:
    name: str
    current_rate: int


@dataclass
class InputSubscriptionMessage:
    type: str
    value: str
