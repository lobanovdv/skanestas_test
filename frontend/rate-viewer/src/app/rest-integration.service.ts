import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestIntegrationService {

  constructor(private http: HttpClient) { }

  getTicktesResponse(): Observable<HttpResponse<any>> {
    return this.http.get(
      'http://localhost:8000/get_tickets', { observe: 'response' });
  }
}
