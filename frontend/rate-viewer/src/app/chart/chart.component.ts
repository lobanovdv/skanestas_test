import { Component, OnInit } from '@angular/core';
import {WsServiceService} from "../ws-service.service";

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.sass']
})
export class ChartComponent{

  dataPoints:any = [];
  chart:any;
  protected oldSubscription: string|null = null;

  constructor(private wsService: WsServiceService) {
  }

  ngOnInit() {
    this.wsService.messages.subscribe((msg:string) => {

      let data = JSON.parse(msg);
      if (data.name != this.oldSubscription) {
        console.log("clear")
        this.dataPoints.length = 0
        this.chart.render();
        this.oldSubscription = data.name;
      }
      this.dataPoints.push({x: new Date(), y: data.current_rate} );
      console.log("push")
      this.chart.render();
    });
  }

  chartOptions = {
    theme: "light2",
    zoomEnabled: true,
    exportEnabled: true,
    title: {
      text:"Rate"
    },
    subtitles: [{
      text: "Loading Data...",
      fontSize: 24,
      horizontalAlign: "center",
      verticalAlign: "center",
      dockInsidePlotArea: true
    }],
    axisY: {
      title: "value",
      prefix: ""
    },
    data: [{
      type: "line",
      name: "Closing Price",
      yValueFormatString: "#,###",
      xValueType: "number",
      dataPoints: this.dataPoints
    }]
  }

  getChartInstance(chart: object) {
    this.chart = chart;
  }

  ngAfterViewInit() {
    function getRandomInt(min:number, max:number) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min) + min); // The maximum is exclusive and the minimum is inclusive
    }


    var x = 0
    const that = this
    const a = function () {
      x += 1
      that.dataPoints.push({x: new Date(), y: getRandomInt(-50,50)} );
      console.log("push")
      that.chart.render();
      setTimeout(a,1000)
    }
    //a()
    this.chart.subtitles[0].remove();
  }
}
