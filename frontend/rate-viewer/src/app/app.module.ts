import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule} from "@angular/common/http";
import {TicketsDropdownComponent} from "./tickets-dropdown/tickets-dropdown.component";
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatSelectModule } from '@angular/material/select';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import * as CanvasJSAngularChart from '../assets/canvasjs.angular.component';
import { ChartComponent } from './chart/chart.component';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
var CanvasJSChart = CanvasJSAngularChart.CanvasJSChart;

const config: SocketIoConfig = { url: 'ws://localhost:8000', options: {path:"/ws"} };

@NgModule({
  declarations: [
    AppComponent,
    TicketsDropdownComponent,
    CanvasJSChart,
    ChartComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatSelectModule,
    NgxMatSelectSearchModule,
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    NgxMatSelectSearchModule,
    SocketIoModule.forRoot(config),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
