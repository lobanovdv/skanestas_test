import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { takeUntil } from 'rxjs/operators';
import {ReplaySubject, Subject} from 'rxjs';
import {RestIntegrationService} from "../rest-integration.service";
import {WsServiceService} from "../ws-service.service";



@Component({
  selector: 'app-tickets-dropdown',
  templateUrl: './tickets-dropdown.component.html',
  styleUrls: ['./tickets-dropdown.component.scss'],
})
export class TicketsDropdownComponent implements OnInit, OnDestroy {
  protected tickets: any[] = [];
  protected loading: boolean = true;
  protected oldSubscription: string|null = null;

  public ticketCtrl: UntypedFormControl = new UntypedFormControl();
  public ticketFilterCtrl: UntypedFormControl = new UntypedFormControl();
  public filteredTickets: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;

  protected _onDestroy = new Subject<void>();

  constructor(private restIntegration: RestIntegrationService, private wsService: WsServiceService) { }

  ngOnInit() {
    this.restIntegration.getTicktesResponse().subscribe((response:any)=> {
      let result = []
      for (const item of response.body["results"]) {
        result.push({
          name: item["name"],
          id: item["name"]
        })
      }
      this.tickets = result;
      this.loading = false;

      const that = this
      this.ticketCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(function () {
        if (that.oldSubscription != null) {
          that.wsService.messages.next({type: "close", value: that.oldSubscription});
          that.wsService.messages.next({type: "open", value: that.ticketCtrl.value?.name});
        } else {
          that.wsService.messages.next({type: "open", value: that.ticketCtrl.value?.name});
        }
        that.oldSubscription = that.ticketCtrl.value?.name
      })
      this.filteredTickets.next(this.tickets.slice());
      this.ticketFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
          this.filterTickets();
        });

    })

  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected filterTickets() {
    if (!this.tickets) {
      return;
    }
    // get the search keyword
    let search = this.ticketFilterCtrl.value;
    if (!search) {
      this.filteredTickets.next(this.tickets.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredTickets.next(
        this.tickets.filter(ticket => ticket.name.toLowerCase().indexOf(search) > -1)
    );
  }

}
