import { Component } from '@angular/core';
import {RestIntegrationService} from "./rest-integration.service";
import {HttpResponse} from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  constructor(private restIntegration: RestIntegrationService) {}
  ngOnInit() {
    this.restIntegration.getTicktesResponse().subscribe((response:any)=> {
      let result = []
      for (const item of response.body["results"]) {
        result.push({
          name: item["name"],
        })
      }
      this.items = result
    })
  }
  title = 'app';
  items = [
    {
      name: "CASUAL",
    },
    {
      name: "Test",
    },
    {
      name: "test1",
    },
    {
      name: "test2",
    },
  ];
  item =  "test2";


}
