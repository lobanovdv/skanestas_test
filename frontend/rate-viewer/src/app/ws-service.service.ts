import { Injectable } from '@angular/core';
import * as Rx from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class WsServiceService {

  constructor() {
    // @ts-ignore
    this.messages = this.connect().pipe(map(
      (response: MessageEvent) => {
        return response.data;
      }
    ));

  }

  private subject: Rx.Subject<MessageEvent>;
  public messages: Rx.Subject<any>;

  public connect(): Rx.Subject<MessageEvent> {
    if (!this.subject) {
      this.subject = this.create("ws://localhost:8000/ws");
    }
    return this.subject;
  }

  private create(url:string): Rx.Subject<MessageEvent> {
    let ws = new WebSocket(url);

    let observable = Rx.Observable.create((obs: Rx.Observer<MessageEvent>) => {
      ws.onmessage = obs.next.bind(obs);
      ws.onerror = obs.error.bind(obs);
      ws.onclose = obs.complete.bind(obs);
      return ws.close.bind(ws);
    });
    let observer = {
      next: (data: any) => {
        if (ws.readyState === WebSocket.OPEN) {
          ws.send(JSON.stringify(data));
        }
      }
    };
    return Rx.Subject.create(observer, observable);
  }
}
